/** @type {import('tailwindcss').Config} */
module.exports = {
	content: ['./src/**/*.{astro,html,js,jsx,md,mdx,svelte,ts,tsx,vue}'],
	theme: {
		extend: {
			colors: {
				'vb-100': '#EBE6DD',
				'vb-200': '#E6D4C2',
				'vb-900': '#292623',
				'vbg-100': '#7D8E26',
				'vbg-200': '#6A7821',
				'vbr-100': '#E08B6C',
				'vbr-200': '#A84724',
			},
		},
	},
	plugins: [],
}
